﻿using System;
using System.Reflection;

namespace Attributes
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var test = new Test();
            var attribute = (typeof(Test).GetCustomAttribute(typeof(LastChangeAttribute)) as LastChangeAttribute);
            Console.WriteLine($"Login={attribute?.ChangedBy}, Date={attribute?.ChangedAt}, TaskId={attribute?.TaskId}");

            var extAttr = typeof(Test).GetMethod("TestLogin")?.GetCustomAttribute(typeof(ExtendedInfoAttribute)) as
                ExtendedInfoAttribute;

            Console.WriteLine($"{extAttr?.Getter} {extAttr?.Description}");
            
            var taxAttr = typeof(Test).GetField("value").GetCustomAttribute(typeof(TaxAttribute)) as
                TaxAttribute;
            
            Console.WriteLine($"{taxAttr?.TaxFrequency} {taxAttr?.TaxType}");
        }
    }
}