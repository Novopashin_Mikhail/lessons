using System;

namespace Attributes
{
    [LastChange("Alex", "2019-12-02", 100)]
    public class Test
    {
        [Tax(TaxType.Fixed, TaxFrequency.Early)]
        public int value;
        
        [ExtendedInfo(TypeMethod.Getter, "Получает логин")]
        public void TestLogin(string login)
        {
            
        }        
    }
}