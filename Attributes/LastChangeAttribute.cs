using System;

namespace Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LastChangeAttribute : Attribute
    {
        private string _changedBy;
        private DateTime _changeAt;
        private int _taskId;
        
        public LastChangeAttribute(string changedBy, string changedAt, int taskId)
        {
            _changedBy = changedBy;
            _changeAt = DateTime.Parse(changedAt);
            _taskId = taskId;
        }

        public string ChangedBy => _changedBy;
        public DateTime ChangedAt => _changeAt;
        public int TaskId => _taskId;
    }
}