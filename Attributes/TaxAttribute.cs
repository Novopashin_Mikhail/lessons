using System;

namespace Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TaxAttribute : Attribute
    {
        private TaxType _taxType;
        public TaxType TaxType => _taxType;
        private TaxFrequency _taxFrequency;
        public TaxFrequency TaxFrequency => _taxFrequency;
        
        public TaxAttribute(TaxType taxType, TaxFrequency taxFrequency)
        {
            _taxType = taxType;
            _taxFrequency = taxFrequency;
        }
    }

    public enum TaxType
    {
        Fixed,
        Percentage
    }

    public enum TaxFrequency
    {
        Monthly,
        Early
    }
}