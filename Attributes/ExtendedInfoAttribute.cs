using System;
using System.Collections.Generic;

namespace Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ExtendedInfoAttribute : Attribute
    {
        private TypeMethod _typeMethod;
        public TypeMethod Getter => _typeMethod;
        
        private string _description;
        public string Description => _description;
        
        public ExtendedInfoAttribute(TypeMethod typeMethod, string description)
        {
            _typeMethod = typeMethod;
            _description = description;
        }
    }

    public enum TypeMethod
    {
        Getter,
        Setter,
        Boilerplate
    }    
}