using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace Lesson7
{
    public class UrlParser
    {
        public IEnumerable<string> GetUrls(string url)
        {
            var request = WebRequest.Create(url);
            var html = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

            Match m;
            var oldRegex = @"(?<1>(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?)";
            var regex = @"(?<1>(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))(:[0-9]{1,5})?([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?)";

            m = Regex.Match(html, regex, RegexOptions.IgnoreCase | RegexOptions.Compiled, TimeSpan.FromSeconds(1));
            while (m.Success)
            {
                var group = m.Groups[1];
                yield return group.Value;
                m = m.NextMatch();
            }
        }
    }
}