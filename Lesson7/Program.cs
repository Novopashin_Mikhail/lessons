﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace Lesson7
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var endApp = false;
            var parser = new UrlParser();

            while (!endApp)
            {
                var url = string.Empty;
                Console.WriteLine("Введите url сайта, пример https://yandex.ru/");
                url = Console.ReadLine();

                var urls = parser.GetUrls(url);
                foreach (var item in urls)
                {
                    Console.WriteLine(item);
                }
                
                Console.WriteLine("------------------------\n");
                Console.WriteLine("Введите 'n' и нажмите Enter для закрытия приложения");
                if (Console.ReadLine() == "n") endApp = true;
                Console.WriteLine("\n");
            }
        }
    }
}