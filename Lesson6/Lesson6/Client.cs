using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson6
{
    public class Client
    {
        private List<Account> Accounts { get; set; }
        private List<User> Users { get; set; }
        private List<History> Histories { get; set; }
        
        public Client BuildLists()
        {
            var startedAt1 = new DateTime(2009, 6, 20);
            var startedAt2 = new DateTime(2010, 7, 20);
            Accounts = new List<Account>
            {
                new Account {Id = 1, Balance = 100000, StartedAt = startedAt1, UserId = 1},
                new Account {Id = 2, Balance = 80000, StartedAt = startedAt1, UserId = 1},
                new Account {Id = 3, Balance = 150000, StartedAt = startedAt2, UserId = 2},
            };

            Users = new List<User>
            {
                new User
                {
                    Id = 1, Login = "user1", Passport = "passport1", Password = "123", FirstName = "Maxim",
                    LastName = "Maximov", SecondName = "Maximovich", PhoneNumber = "89053334455",
                    RegistrationAt = startedAt1
                },
                new User
                {
                    Id = 2, Login = "user2", Passport = "passport2", Password = "123", FirstName = "Mikhail",
                    LastName = "Mikhailovich", SecondName = "Mikhailov", PhoneNumber = "89056664455",
                    RegistrationAt = startedAt2
                }
            };

            Histories = new List<History>
            {
                new History {Id = 1, OperationType = OperationType.Input, AccountId = 1, Balance = 150000},
                new History {Id = 2, OperationType = OperationType.Output, AccountId = 1, Balance = 50000},
                new History {Id = 3, OperationType = OperationType.Input, AccountId = 2, Balance = 80000},
                new History {Id = 4, OperationType = OperationType.Input, AccountId = 3, Balance = 200000},
                new History {Id = 5, OperationType = OperationType.Output, AccountId = 3, Balance = 50000}
            };

            return this;
        }

        public Account GetAccount(int accountId, string login, string password)
        {
            var accounts = from account in Accounts
                let userIds = from usr in Users where usr.Login == login && usr.Password == password select usr.Id
                where userIds.FirstOrDefault() == account.UserId && account.Id == accountId
                select account;
            return accounts.FirstOrDefault();
        }
        
        public List<Account> GetAccounts(string login, string password)
        {
            var accounts = from account in Accounts
                let userIds = from usr in Users where usr.Login == login && usr.Password == password select usr.Id
                where userIds.FirstOrDefault() == account.UserId
                select account;
            return accounts.ToList();
        }
        
        public List<Account> GetAccountsWithHistory(string login, string password)
        {
            var accounts = from account in Accounts
                    let userIds = from usr in Users where usr.Login == login && usr.Password == password select usr.Id
                    where userIds.FirstOrDefault() == account.UserId
                    select account;

            foreach (var account in accounts)
            {
                var history = from h in Histories
                    where h.AccountId == account.Id
                    select h;

                if (history.Any())
                {
                    if (account.History == null) account.History = new List<History>();                    
                    account.History.AddRange(history);
                }
            }
            
            return accounts.ToList();
        }

        public string GetInputOperation()
        {
            var history = from h in Histories
                where h.OperationType == OperationType.Input
                select h;

            var accounts = from acc in Accounts
                where history.Select(hh => hh.AccountId).Contains(acc.Id)
                select acc;
            
            foreach (var account in accounts)
            {
                var historyInner = history.Where(hh => hh.AccountId == account.Id);
                if (historyInner.Any())
                {
                    if (account.History == null) account.History = new List<History>();                    
                    account.History.AddRange(historyInner);
                }
            }

            var users = from usr in Users
                where accounts.Select(a => a.UserId).Contains(usr.Id)
                select usr;
            
            foreach (var user in users)
            {
                var accountInner = accounts.Where(a => a.UserId == user.Id);
                if (accountInner.Any())
                {
                    if (user.Accounts == null) user.Accounts = new List<Account>();                    
                    user.Accounts.AddRange(accountInner);
                }
            }

            var resH = users.SelectMany(x => x.Accounts.SelectMany(y => y.History).Select(r => $"ФИО:{x.LastName}{x.FirstName}{x.SecondName} Id_счета:{r.Id}"));
            var sb = new StringBuilder();
            foreach (var h in resH)
            {
                sb.AppendLine(h);
            }

            return sb.ToString();
        }

        public List<User> GetUsersByBalance(double balance)
        {
            var accounts = from acc in Accounts where acc.Balance >= balance select acc;
            
            var users = from usr in Users
                where accounts.Select(a => a.UserId).Contains(usr.Id)
                select usr;

            return users.Distinct().ToList();
        }
    }
}