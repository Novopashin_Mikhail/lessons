using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson6
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondName { get; set; }
        public string PhoneNumber { get; set; }
        public string Passport { get; set; }
        public DateTime RegistrationAt { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<Account> Accounts { get; set; }
        public override string ToString()
        {
            var baseInfo = $"Id={Id};Имя={FirstName};Отчество={SecondName};Фамилия={LastName};Телефон={PhoneNumber}," +
                   $"Паспорт={Passport};ДатаРегистрации={RegistrationAt};Логин={Login};Пароль={Password}";

            if (Accounts != null && Accounts.Any())
            {
                baseInfo += Environment.NewLine;
                foreach (var account in Accounts)
                {
                    baseInfo += account.ToString();
                }
            }

            return baseInfo;
        }
    }
}