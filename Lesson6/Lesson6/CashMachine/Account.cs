using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson6
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime StartedAt { get; set; }
        public double Balance { get; set; }
        public int UserId { get; set; }
        
        public List<History> History { get; set; }

        public override string ToString()
        {
            var baseInfo = $"Id={Id};ДатаОткрытияСчета={StartedAt};Сумма={Balance};UserId={UserId}";
            if (History != null && History.Any())
            {
                baseInfo += Environment.NewLine;
                foreach (var item in History)
                {
                    baseInfo += $"\t" + item;
                    baseInfo += Environment.NewLine;
                }
            }

            return baseInfo;
        }
    }
}