using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace Lesson6
{
    public class History
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public OperationType OperationType { get; set; }
        public double Balance { get; set; }
        public int AccountId { get; set; }

        public override string ToString()
        {
            return
                $"Id={Id};ДатаОперации={CreatedAt};ТипОперации={OperationType.GetDescription()};" +
                $"Сумма={Balance};AccountId={AccountId}";
        }
    }

    public enum OperationType
    {
        [Description("Пополнение")]
        Input = 0,
        [Description("Списание")]
        Output = 1
    }
}