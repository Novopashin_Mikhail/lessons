﻿using System;

namespace Lesson6
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var client = new Client();
            client.BuildLists();
            
            var account = client.GetAccount(1, "user1", "123");
            Console.WriteLine(account);
            
            var accounts = client.GetAccounts("user1", "123");
            foreach (var acc in accounts)
            {
                Console.WriteLine(acc);
            }

            var accWithHistory = client.GetAccountsWithHistory("user1", "123");
            foreach (var item in accWithHistory)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(client.GetInputOperation());

            var users = client.GetUsersByBalance(110000);
            foreach (var user in users)
            {
                Console.WriteLine(user);
            }
        }
    }
}