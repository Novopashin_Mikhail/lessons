﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Lesson6.Test
{
    [TestFixture]
    public class Tests
    {
        private Client _client;
        public Tests()
        {
            _client = new Client();
            _client.BuildLists();
        }
        [Test]
        public void Test1()
        {
            var account = _client.GetAccount(1, "user1", "123");
            Assert.AreEqual(account.Id, 1);
        }
        
        [Test]
        public void Test2()
        {
            var accounts = _client.GetAccounts("user1", "123");
            Assert.AreEqual(accounts.Count, 2);
        }
        
        [Test]
        public void Test3()
        {
            Assert.AreEqual(_client.GetInputOperation().Contains("ФИО:MaximovMaximMaximovich Id_счета:3"), true);
        }
    }
}