using System;
using System.Linq;
using System.Reflection;

namespace Lesson8
{
    public class Serializator<T>
        where T: new()
    {
        public static string Serialize<T>(T obj)
        {
            var fieldInfo = GetFieldsByReflection();
            var values = fieldInfo.Select(f => $"{f.Name}:{f.GetValue(obj)}");
            return String.Join(",", values);
        }

        public static T Deserialize(string str)
        {
            var obj = new T();
            var fieldsInfo = GetFieldsByReflection();
            var fields = str.Split(',');
            if (!fields.Any()) return default;
            foreach (var field in fields)
            {
                var keyValue = field.Split(':');
                var fieldInfo = fieldsInfo.FirstOrDefault(x => x.Name == keyValue[0]);
                if (fieldInfo == null) return default;
                fieldInfo.SetValue(obj, Convert.ChangeType(keyValue[1], fieldInfo.FieldType));
            }

            return obj;
        }

        static FieldInfo[] GetFieldsByReflection()
        {
            var type = typeof(T);
            return type.GetFields(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
        }
    }
}