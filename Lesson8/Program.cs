﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;

namespace Lesson8
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var f = new F{ i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
            var sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 100_000; i++)
            {
                Serializator<F>.Serialize(f);//00:00:00.2577360
            }
            sw.Stop();
            Console.WriteLine($"MyReflection: 100_000 итераций сериализации выполнено за {sw.Elapsed}");
            Console.WriteLine("--------------------------------");

            sw.Reset();
            var s = Serializator<F>.Serialize(f);
            sw.Start();
            for (int i = 0; i < 100_000; i++)
            {
                var obj = Serializator<F>.Deserialize(s);
            }
            sw.Stop();
            Console.WriteLine($"MyReflection: 100_000 итераций десериализации выполнено за {sw.Elapsed}");
            Console.WriteLine("--------------------------------");
            
            sw.Reset();
            sw.Start();
            Console.WriteLine(Serializator<F>.Serialize(f));
            sw.Stop();
            Console.WriteLine($"Вывод строки в консоль занимает {sw.Elapsed}");
            Console.WriteLine("--------------------------------");

            sw.Reset();
            sw.Start();
            for (int i = 0; i < 100_000; i++)
            {
                s = JsonConvert.SerializeObject(f);                
            }
            sw.Stop();
            Console.WriteLine($"NewtonsoftReflection: 100_000 итераций сериализации выполнено за {sw.Elapsed}");
            Console.WriteLine("--------------------------------");
            
            sw.Reset();
            sw.Start();
            for (int i = 0; i < 100_000; i++)
            {
                var objNewtonsoft = JsonConvert.DeserializeObject<F>(s);                
            }
            sw.Stop();
            Console.WriteLine($"NewtonsoftReflection: 100_000 итераций десериализации выполнено за {sw.Elapsed}");
        }
    }
}